# Abstract

Mental health related diagnoses have been on the rise these last years, especially since the pandemic. In the CELEHS laboratory, we analyze electronic health records to help clinicians identify at-risk patients requiring follow-up.

In this talk I will present the results of Glove word embeddings on 8,000 open-access suicide-related publications, using the text2vec, opticskxi and sigmagraph R packages. I developed a novel methodology based on random projections to efficiently find diverse clusters of related concepts in unstructured text data, and evaluate the results by predicting pairs of related concepts, and comparing them to clinician-based known relationships. While many biomedical natural language processing approaches focus on the analysis of specific known concepts, as the ones indexed by the Unified Medical Language System (UMLS), the analysis of the complete text can enable to find novel relationships and borderline concepts.

The Diagnostic and Statistical Manual of Mental Disorders (DSM) is the main reference for clinicians to diagnose mental health diseases, and describes sets of symptoms that form the required diagnostic criteria for each disease. The DSM emphasizes that many patients are diagnosed with multiple conditions, and that current diagnoses could benefit from introducing multidimensional assessments, by taking into account the severity, intensity, duration, and combinations of symptoms, to form more precise diagnostics and help treatment. The DSM also underlines the necessity for diagnoses that take into account the spectrum and gradients of disorders observed, as in schizoaffective and autism spectrum disorders. To this end, the analysis of unstructured text data can help identify clusters of conditions and enable new multidimensional classifications of mental health disorders.

My talk will first present an overview of the problematic as underlined in the DSM and introduce Glove word embeddings using the text2vec package on a set of 8,000 open-access suicide-related publications. I then demonstrate how to explore the embeddings using vector operations to manually find clusters of related concepts, and in a second step automate the discovery of such clusters using the density-based clustering package opticskxi, and visualize the clusters as graphs using the sigmagraph network visualization package. Further clusters are then discovered by applying semi-directed vector operations, a novel method inspired by random projections. In a last step, I introduce ways to evaluate such clusters, using a database of 17,000 known concepts pairs curated by clinicians with expert knowledge, by predicting pairs of related concepts using a false positive threshold cut-off on cosine similarities.

Novel methodologies in natural language processing will enable us to further understand mental health disorders and their interactions. Specific disorders have been associated to personality traits, as schizophrenia with neuroticism and autism with obsessive-compulsive, and the modeling of such interactions and the further discovery of novel interactions will enable us to enhance the treatment of mental health disorders and identify clinically-actionable features.

# Getting started

Start by setting up the computing environment and downloading the required data

## Setting up the computing environment

You can either use the provided Docker image, or install software in your R console

### Docker image

```
docker pull thomaschln/knowledgegraphs:main

# minimal docker run command, no plots
docker run -it thomaschln/knowledgegraphs:main R

# my usual docker command, with plots and local directory
docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v ~/.Xauthority:/root/.Xauthority:rw --net host -v `pwd`:`pwd` -w `pwd` thomaschln/knowledgegraphs:main R
```

### Manual installation in R console (root for system-level dependencies)

```
# you might need to install these system-level dependencies of igraph
# ignore if you already have igraph
system('apt-get update && apt-get install -y libxml2-dev libglpk-dev')

# these are some of the main packages imported by the packages I developed
install.packages(c('dplyr', 'reshape2', 'RColorBrewer', 'pROC', 'text2vec', 'flexdashboard', 'tidypmc', 'tm', 'R.utils', 'igraph'))

# this one is just for this vignette
install.packages('europepmc')

# my good ol' density-based clustering package, use dev version for latest features
# install.packages('opticskxi', dependencies = TRUE)
remotes::install_git('https://www.gitlab.com/thomaschln/opticskxi', dependencies = TRUE)

# my new network viz package, recently added to CRAN
# install.packages('sgraph')
remotes::install_git('https://www.gitlab.com/thomaschln/sgraph')

# my latest package to build knowledge graphs, soon to be submitted to CRAN
remotes::install_git('https://www.gitlab.com/thomaschln/kgraph')

# my very early package to perform NLP embeddings
remotes::install_git('https://www.gitlab.com/thomaschln/nlpembeds')

# the public version of my project-specific R package
remotes::install_git('https://www.gitlab.com/thomaschln/psychclust_rmed24')
```

## Downloading open-access publications

### Using the europepmc package

We can download full-text publications using the 'europepmc' package.
A search mechanism is provided, e.g. to obtain all documents with the string 'suicidal ideation' in the title.

```
library(magrittr)
library(tidyverse)
library(europepmc)

# 1362 records
suic_docs_ids <- europepmc::epmc_search("title:(suicidal ideation) OPEN_ACCESS:Y")

# some idxs may be NA
docs <- suic_docs_ids$pmcid %>% head %>% na.omit %>% purrr::map(europepmc::epmc_ftxt)
```

The 'tidypmc' package enables to parse the XML file

```
ftext = lapply(docs, tidypmc::pmc_text)
```

### Using the FTP

At this URL, we can find all PMC open-access publications.

```
https://europepmc.org/ftp/oa/
```

We can then locally parse the files using 'tidypmc'.

### Pre-built databases

I provide two databases I created. The first one is a collection of 1,700 publications with the pattern 'suicid' (to include denonyms i.e. both 'suicide' and 'suicidal') in the title, created using ~20% of the total publications.

```
# epmc_1700
```

[Dropbox folder](https://www.dropbox.com/scl/fo/d2450t308jab8867k5crv/ADGCTETG6eAlIKIPdKiWtkc?rlkey=8c2umdh3bmtdhyw2ohqgigjdc&st=ppv41nqm&dl=0)

The second one is a collection of 8,000 publications with the pattern 'suicid' in the title, created using ~99% of the publications (the few files >215Mb were not processed as they required >16Gb RAM).

```
# epmc
```

Both databases are reproducible using the 'psychclust' package (cf. Makefile rules download\_extdata and db0). If you have less than 8Gb RAM on your machine, it's probably best to use only the smaller version.

# First steps

## Importing the XML file into R

We start by creating an R object from the XML file, using the tidypmc package.

```
  # if using local R console replace dirpath by where you saved the file
  dirpath = system.file('extdata', package = 'psychclustRMED24')
  fpath = file.path(dirpath, 'epmc_1700_suic_db.xml')

  # this shouldn't take more than a few minutes on modern hardware,
  # otherwise download the glove_fit file below
  ftext = nlpembeds:::build_ftext_from_pmcxml(fpath, '<articles>', '</articles>')
```

## Text processing

We want to sanitize slightly the publications first and make sure it is in the good format to be fed to 'text2vec'. In the 'process\_pubmed' function, we remove irrelevant sections, table and figure references, and some (but not all) punctuation. In the `prune_text` function we start using 'text2vec' to prune the vocabulary (remove terms that appear less than 3 times in all publications) ad remove stopwords using the list provided by the 'tm' package. We can also perform stemming, but it is quite long to perform as it is not optimized (3 mins on 1700, >1 hour on 8000).

```
  processed_text = nlpembeds:::process_pubmed(ftext)

  # this shouldn't take more than a few minutes on modern hardware,
  # otherwise download the glove_fit file below
  pruned_text = nlpembeds:::prune_text(processed_text, stem_derivatives = FALSE)
```

## Word embeddings

The functions create a pruned version of the full texts, then call 'text2vec' using the str\_to\_vectors function, which is a wrapper to perform Glove word embeddings as described in the 'text2vec' vignette. The default parameters are 10 word windows (in both directions, thus 20), and an output number of dimensions of 100. The output files are saved as R objects, so we can easily investigate them further.

```{r}
  glove_fit = nlpembeds:::str_to_vectors(pruned_text)

  # if anything failed up to here, download and import this file to continue exploration
  # system('wget -O glove_fit.rds "https://www.dropbox.com/scl/fi/hfhz4ckoafy2qrtvaq9is/epmc_1700_glove_fit.rds?rlkey=6gls7prysca0y7mdyyhnqb6qg&st=u256b9zw&dl=0"')
  # glove_fit = get(load('glove_fit.rds'))
```

# Exploration of the embeddings

Now that we have our fitted embeddings, we can start exploring our results.

## Terms queries

We can query a single word or two related terms using the vector\_operation function. The cosine similarity is computed between the embedding of the query on the 100 dimensions, against all other words, and the closest neighbors are returned.

```
nlpembeds:::vector_operation(glove_fit, 'suicide', n_closest = 50)
nlpembeds:::vector_operation(glove_fit, c('suicidal', 'ideation'), n_closest = 50)
```

We want to explore our embeddings to ultimately map a knowledge graph for mental health, to show how different concepts are related, how they interact, which are closer to one another, and how they cluster into groups. Our ultimate goal is to identify predictors of suicide risk, to enable clinicians to follow-up in priority on at-risk patients. More generally, we would also like our analyses to enable shared decision making, to help directly the clinician make a diagnosis and to help recommend efficient treatments and prevention strategies. 

We can have a look at therapies

```
nlpembeds:::vector_operation(glove_fit, 'therapy', n_closest = 50)
# psychotherapy, cbt, dbt, behavioral, dialectical
```

Or medications (we can notice terms related to substance use)

```
nlpembeds:::vector_operation(glove_fit, 'medication', n_closest = 50)
# psychotropic, antidepressant, antipsychotic, anxiolytic, benzodiazepine
# Substance use terms: overdose, alcohol, opioid
```

Or risk factors (results are not great, too general)

```
nlpembeds:::vector_operation(glove_fit, c('suicide', 'risk'), n_closest = 50)
nlpembeds:::vector_operation(glove_fit, c('risk', 'factor'), n_closest = 50)
```

## Vector operations

One powerful feature of embeddings is the ability to subtract terms. Instead of only looking at terms associated with suicide risk, we can also look at which terms are not associated with treatments, and vice-versa.

```   
nlpembeds:::vector_operation(glove_fit, c('suicide', 'medication'), 'risk', n_closest = 50)
# 'alcohol' and 'opioid' were removed
# some "negative" drugs remain (pesticide, self-poisoning) but they are rather suicide means than risk factors

nlpembeds:::vector_operation(glove_fit, c('suicide', 'risk'), 'medication', n_closest = 50)
# still not great, still too general
```

One of our research questions is: what makes an individual go from suicidal ideation to a suicide attempt ?

```
nlpembeds:::vector_operation(glove_fit, 'si', 'sa', n_closest = 50)
nlpembeds:::vector_operation(glove_fit, 'ideation', 'attempt', n_closest = 50)
nlpembeds:::vector_operation(glove_fit, 'attempt', 'ideation', n_closest = 50)
```

One borderline concept is non-suicidal self-injury (NSSI). Recognized by clincians as closer to an addiction, it can however predate suicide attempts, especially when multiple methods of self-injury are performed. Also, patients may sometimes deny suicidal intent.

We might want to discover what distinguishes NSSI from injuries due to suicide attempts:

```
nlpembeds:::vector_operation(glove_fit, 'nssi', 'injury', n_closest = 50)
```

In the context of mental health, we know that veterans may suffer from PTSD, and we would like to understand how it interacts with suicidal ideation.

```
nlpembeds:::vector_operation(glove_fit, c('attempt', 'ptsd'), 'ideation', n_closest = 50)
nlpembeds:::vector_operation(glove_fit, c('ideation', 'ptsd'), 'attempt', n_closest = 50)
```

Results can vary slightly as parameters of the pipeline are modified. One term that caught my attention was emotional intelligence (EI).

```
nlpembeds:::vector_operation(glove_fit, 'ei', n_closest = 50)
```

Another important complex mental health diagnosis is autism spectrum disorder (less related to suicidality than bipolar disorder or schizophrenia, but with "similarities" with PTSD).

```
nlpembeds:::vector_operation(glove_fit, 'asd', n_closest = 50)
```

## Concepts subspaces

Vector operations enable to exhibit "concepts subspaces".

A well-known example on general text corpora is the operation "Paris - France + Germany = Berlin". By querying the two terms 'Paris' and 'Germany' and subtracting 'France', the closest neighbor is Berlin. But more interestingly, when we consider the top 10 neighbors, many other capitals appear. This highlights the fact that the vector operation projects the words in a "capital subspace". By framing correctly vector operations, we can make subspaces of related concepts appear.

Although since a few years PTSD can also be diagnosed in youth and civilian victims of abuse, in the overall publications it remains strongly associated with military veterans. One naive hypothesis to frame a vector operation in the same way as the capitals example could be: "ASD is to children what PTSD is to veterans". Building on "Paris is to France what Berlin is to Germany", the example becomes: "Veterans - PTSD + ASD"

```
nlpembeds:::vector_operation(glove_fit, c('veterans', 'asd'), 'ptsd', n_closest = 50)
# socio-economic subspace: geography, ethnicity, age, gender, occupation
```

Another way to formulate it could be "PTSD - veterans + children"

```
nlpembeds:::vector_operation(glove_fit, c('ptsd', 'children'), 'veterans', n_closest = 50)     
# psychiatric diagnoses subspace
```

Another interesting related operation: "Trauma - veterans + children"

```
nlpembeds:::vector_operation(glove_fit, c('trauma', 'children'), 'veterans', n_closest = 50)   
# "personal history" subspace: violence, abuse, maltreatment
```

## Conclusions

The embeddings show some expected groups of concepts, e.g. medications (antidepressants, antipsychotics...), therapies (behavioral, dialectical...), and some coherent associations between personality traits and disorders (schizophrenia linked to neuroticism, autism to obessive-compulsive). Some medical symptoms are particularly interesting (e.g. tinnitus, alexithmia) as they could provide more objective assessments for clinicians to build upon. However, the fact that a word appears in the nearest neighbors of others may be due to a spurious correlation, and we want to make sure we are not cherry-picking nice-looking results. Thus, we want to rely on more objective and automatic group discovery methods, as clustering. Since the concepts have blurred boundaries, we want to first focus on core groups well-separated from others and ignore in-between concepts, thus we apply density-based clustering.

To help guide our clustering and to avoid requiring too much computational resources, we use the above operations to build a list of relevant features. We select ~20 operations, which each return the 50 nearest features, which built a list of ~800 unique features. We thus have a data matrix of ~800 words by 100 Glove dimensions which we use as input for density-based clustering.

# Confirmation

## Density-based clustering

### DBSCAN, OPTICS, OPTICS k-Xi

To introduce density-based clustering methods, we can have a look at the [optickxi R package vignette](https://cran.r-project.org/web/packages/opticskxi/vignettes/opticskxi.pdf)

### OPTICS k-Xi pipeline

In the above vignette the OPTICS k-Xi pipeline is introduced, which enables to compare several models with varying parameters (distance, dimensionality reduction, number of clusters) and select the best model based on distance-based metrics (average silhouette width, between-within ration, widest gap). 
First published in 2019, the opticskxi package has been updated to enable ensemble metrics (i.e. instead of selecting the best model based on one metric, we select the model best ranked on average in 8 metrics) which increased the overall quality and stability of the clusters.
The `psych_kxi_pipeline` function is an example of a modified OPTICS k-Xi pipeline that uses ensemble metrics, removes too large clusters, and uses cosine similarity.

We first subset our data matrix by calling 23 vector operations, returning each 50 closest neighbors, producing a list of ~800 unique words.

```
  list_of_words = psychclustRMED24:::get_list_of_words_metadf(glove_fit)
  m_words = glove_fit[list_of_words, ]
```

The function `psych_kxi_pipeline` is a modified version of `opticskxi_pipeline` to remove huge clusters and use cosine similarity.

```
  df_kxi = opticskxi:::psych_kxi_pipeline(m_words, n_cores = 1)
```

We can use the classic opticskxi functions on it

```
  best_kxi = opticskxi::get_best_kxi(df_kxi)

  # OPTICS reachability plot colored by clusters
  best_kxi %$% opticskxi::ggplot_optics(optics, groups = clusters)

  # Multiple reachability plots
  opticskxi::gtable_kxi_profiles(df_kxi) %>% plot

  # The default metric is 'avg.silwidth', we can change to 8 others
  names(df_kxi$metrics[[1]])

  # The Dunn index prefers well-separated clusters
  opticskxi::gtable_kxi_profiles(df_kxi, 'dunn') %>% plot
```

I found the clusters weren't very stable, so this motivated me to implement a long-planned feature: ensemble metrics ! Instead of selecting one metric, we take the models that have best rank overall on several metrics.

```
  df_kxi = opticskxi:::psych_kxi_ensemble_models(m_words)

  opticskxi::gtable_kxi_profiles(df_kxi) %>% plot
```

We can visualize it on ICA:

```
  best_clusters = df_kxi$clusters[[1]]

  colnames(m_words) = seq_len(ncol(m_words))
  opticskxi::fortify_ica(m_words, n.comp = 4, sup_vars = data.frame(Clusters = best_clusters)) %>%
    opticskxi::ggpairs('Clusters', axes = 1:4, ellipses = TRUE) %>% plot
```

But it is not very helpful (and t-SNE also isn't great). This is where nearest neighbors graphs come to the rescue.

### Nearest neighbors graphs

Nearest neighbors graphs enable to visualize how these clusters connect between each other. We build them using sparse codings: for each word, we keep only the 5 or 10 nearest neighbors, and all other values are set to 0. Then, in the graph, each word is a node and is connected to it's 5 or 10 nearest neighbors. The nodes are colored by the OPTICS k-Xi clusters, and the graph is visualized using the 'sgraph' package. Nearest neighbors graphs enable to visualize this kind of noisy high-dimensional data much more efficiently than classic dimensionality reduction methods as PCA, ICA, or t-SNE.

```
  m_words = m_words[!is.na(best_clusters), ]                               
  best_clusters %<>% na.omit                                               

  m_dist = 1 - text2vec::sim2(m_words)
  diag(m_dist) <- NA
  sp_enc <- apply(m_dist, 2, nlpembeds:::sparse_encode_vec, 'hard', n_neighbors = 10)

  df_enc = reshape2::melt(sp_enc) %>% subset(value != 0)
  words_graph = list(df_links = df_enc[1:2],
    df_nodes = data.frame(id = rownames(m_words), clusters = best_clusters))

  words_graph$df_nodes$labels = words_graph$df_nodes$id

  igraph = sgraph::l_graph_to_igraph(words_graph)
```

Now, we could plot the igraph object, but SigmaJS is much cooler and interactive. If you were in your R console, this should work and open a tab in your browser:

```
  sgraph::sgraph_clusters(igraph, label = 'labels', node_size = 6)
```

In my project-specific Shiny apps, I added some parameters to simplify the graph to make reading easier. First we can show all nodes, or only nodes that were assigned to clusters. Second, we can filter nodes by their connectivity (every node has at least 10 edges, and we can select only nodes with e.g. >15 edges).

Shiny saves me a lot of time to explore the influence of parameters on complex models, and I often do my exploration with it, before coming back to the code with specific parameters.

The app is copied in the shiny-server folder but I didn't take the time to make it runnable in this public version, as it requires temporary objects, private databases etc. but you can have a look at it.

### Conclusions

The OPTICS reachability plots reveal 2 clinically-relevant clusters which each have several nested clusters.
* The core of the first cluster is the "disorders" diagnoses. A first peripheral cluster has borderline diagnoses as schizoaffective, ASD, and some disorder-associated personality traits as neuroticism and obsessive-compulsive. The second peripheral cluster has other personality traits and emotionally-related terms.
* The core of the second cluster is the medications, and the peripheral cluster is the therapies.

The nearest neighbor graphs show how in one case, in the diagnoses cluster, the peripheral cluster surround the core cluster, while in in the other case, the medications and therapies are well segregated. This highlights the complex interactions at play in the diagnoses clusters.

We have shown that density-based clustering can identify the concepts clusters automatically. We would now like to attempt to discover further concepts outside of the list of our initial 800 words to:
* Expand our core clusters
* Discover other clusters

## Automated discovery of concepts' clusters

Several approaches could be considered:
* Selecting initial words based on TFIDF
* Selecting words from medically-relevant ontologies
* Applying dimensionality reduction

Since the "subspace vector operations" worked well, we choose to focus on that approach. Essentially, it is performing projections, and according to information theory, random projections can sometimes work better than dimensionality reduction projections as PCA and ICA. PCA constrains axes to be orthogonal and is efficient to find the largest sources of variance in the data, but is limited when there are multiple competing signals we want to consider. Similarly, ICA constrains axes to be independent which can be a more appropriate assumption than PCA, but in real-world data this might still be to constraining. Thus PCA and ICA have constraints that might not represent appropriately the underlying data. This is why performing random projections is useful, however one drawback is the large search space in high-dimensional data. To alleviate this issue, we choose to use semi-directed random projections based on subspace vector operations.

### Semi-directed random vector operations rationale

We have seen that performing the operation "France - Germany + Paris" enables us to be projected in a "capitals subspace". If we had only typed "Paris" we would have got information related to cities, but also countries. Performing "France - Germany" enables us to get rid of the commonalities / noise between the two, e.g. both are nouns, both are geographic regions. By performing "Country1 - Country2" we obtain a representation of what differentiates two countries, independently of the countries considered (although of course some biases remain, e.g. both are European countries). Since by adding "Paris" we get projected in the "capitals subspace", we can easily imagine in another way that by adding a typical culinary specialty would project us into a "culinary subspace". Less intuitively, we would probably still be projected in the "capitals subspace" even if we added a capital from another country, e.g. Moscow.

Since we have discovered core clusters, and that we have concepts that have ambiguous memberships as they rely either on the periphery of a cluster, or in-between two clusters, we can try to find new clusters into which those concepts would fit better, either expanding existing clusters, or discovering totally new clusters. We have seen that "veterans - children + ASD" projects us into a "psychiatric diagnoses subspace", and that "ASD - PTSD + veterans" projects us into a socio-economic subspace. Thus, following on the example "neuroticism is to schizophrenia what obsessive-compulsive is to autism", performing an operation as "schizophrenia - autism + obsessive-compulsive" should be able to project us into a "personality traits subspace" which would include neuroticism, but perhaps also other suicide-relevant personality traits.

In our previous results, we had ~200 words that were clustered. Our strategy is to go over each word, subtract from it a random word **from the same cluster**, add a random word **from any other cluster**, and extract the top 20 matches. Thus, we don't perform our search totally at random, which would probably lose us in non-clinically-relevant terms, and we furthermore help our random projections by performing subtractions between elements of the same cluster. Although many of the returned might already be present in our list, we might be lucky enough to randomly fall on borderline words which will project us into new yet undiscovered clusters. We could improve this by only considering borderline concepts for additions, but let's already start with this simple version. This version could also enable us to "dig into" existing clusters and find more relevant terms within already discovered clusters. Additionally, we could also consider only subtracting elements from core clusters.

### Implementation

We call the `random_vectors` function on an embedding matrix and a clusters data frame.

```
  df_clusters = best_clusters %>%                                
    data.frame(name = rownames(m_words), clusters = .) %>% na.omit              

  randvec_words = nlpembeds:::random_vectors(glove_fit, df_clusters)
```

Here we already have 1800 words, which is approx the maximum for opticskxi to be efficient. So to be able to chain several calls, we want to postprocess the words by removing the rare terms.

```
  # terms_weights not calculated in this demo
  # have a look at the code if interested
  # db_name enables to save temporary objects

  # randvec_words %<>% randvec_postprocess(db_name, glove_fit, terms_weights)
```

We can then subset our `glove_fit` object, refit density-based clusters, re-call random vectors, etc. Here we show results on one pass.

```
  m_words = glove_fit[randvec_words, ]
```

### Results

First results increased the separation between the therapies and medications. Previously, their separation was not very stable, i.e. changing slightly some parameters could either separate them or merge them into the same cluster. Applying the random vector operations on 200 words and extracting 20 nearest neighbors produced ~1,700 words, which starts to be close to the upper bound of the number of features opticskxi can efficiently manage.

We would like to chain several passes of this algorithm consisting of basically two steps:
1. Apply density-based clustering
2. Apply random vector operations

The rapid increase of words considered forces to perform more stemming in initial stages, to increase the quality of the results, and also to remove stop words. But we need another way to reduce the number of features selected at each pass. To that end, we perform 2 main feature selection steps:
1. We remove the 10% less frequent terms
2. We build the nearest neighbor graph and keep only nodes with >15 edges (with a 10 neighbor coding)

This enables us to chain up to 10 pass and grow slowly to 1,300 words. Impressive results are obtained:
* By 2-3 passes, new serum-related cluster appears (cytokines, proteins)
* By 5-6 passes, new cluster of neuroanatomical regions
* On another corpora we analyzed, it also made appear a sleep-related cluster (insomnia, sleepwalking...)

### Discussion

One drawback is the fact the multiple socio-economic clusters are created (an arguable drawback, although less clinically-relevant), and that original clusters such as psychiatric diagnoses become relegated to the noise in the density-based clustering. Moving forward, we would need to think about how to conclude the algorithm:
* We could either apply a mixture clustering method as Gaussian Mixture Clustering, to avoid having a noise concept while still considering borderline concepts and cluster peripheries
* Otherwise we could also merge the knowledge discovered along each pass, as we may not want to rely only on the last set of features

In the meantime, at the CELEHS laboratory, we use the union of all words that were clustered at least once, to build a dictionary of relevant words, that we then use as input for our other NLP algorithms.

In the next section, I will show the evaluation methods we use for our other NLP algorithms, which could also be applied to evaluate the random vector operation cluster discovery.

# Evaluations

The common methodology to evaluate medical machine-learning algorithms is to rely on clinician-curated databases of concept pairs, e.g. disease-disease relations or disease-drug relations. One public database we can use is [PrimeKG](https://github.com/mims-harvard/PrimeKG), published by another Harvard lab led by Prof. Zitnik. Her lab is more focused on bioinformatics drug discovery, as genetic pathways and protein interactions, rather than biomedical hospital applications, as electronic health records and procedures, but it is nonetheless a useful starting point. At CELEHS we also have a larger database of known related concepts pairs focusing on biomedical applications, which is however not public. Thus here is demonstrated the use of [PrimeKG](https://github.com/mims-harvard/PrimeKG).

## Methodology

### Overview

The classic methodology is the following (e.g. [KESER paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8551205/), Hong C. et al., 2021):
1. In the original text data, replace text strings by their unique identifiers (e.g. [UMLS](https://www.nlm.nih.gov/research/umls/index.html) CUIs) and remove all other words
* Replace by largest labels first (there might be one id for "suicide by hanging" and another for "suicide")
* This can be quite long using unoptimized regular expressions. The CELEHS laboratory developed the highly optimized [NILE software](https://celehs.hms.harvard.edu/software/NILE.html) but can be downloaded only using .edu, .org, or .gov email addresses. Thus here we use the unoptimized version from the psychclust package.
2. Compute word embeddings (Glove or SSPMI-SVD)
3. Using known pairs and generated random null pairs, calculate AUC performance score (same number of known and generated null pairs)
4. Determine cosine similarity threshold cut-off at 5 or 10% false positives (doesn't require known pairs, though number of null pairs used is to be considered to have a threshold that is not too lenient nor too stringent)

The [NILE software](https://celehs.hms.harvard.edu/software/NILE.html) contains a 2021 version of [UMLS](https://www.nlm.nih.gov/research/umls/index.html) curated by CELEHS. One can also download the latest version from [UMLS](https://www.nlm.nih.gov/research/umls/index.html) after signing in. Here, we will demonstrate the methodology using [Bioportal ontologies](https://bioportal.bioontology.org/).

### Ontologies

I selected a list of ~25 more or less related to mental health. Out of those, 5 had references to CUIs. In each file, we have parent-child relationships between concepts (e.g. schizophrenia "is a" mental disorder). I performed a propagation of CUIs to child nodes when they were missing to have the most exhaustive coverage possible.

### AUC performance

We then compute AUC using the pROC package to evaluate the quality of our embeddings. AUC 0.7 is competitive with similar medical machine-learning tasks.

### Knowledge graphs

Finally, we determine the 5% false positive threshold on cosine similarity and build the corresponding knowledge graph. We can then specify nodes of interests and make all associated features appear. The graphs' edges are weighted by their cosine similarity (smaller edge = higher similarity). This enables us to visualize the concepts shared by several features of interest.

## Results

### Tutorial

You can have a look at the code of the Shiny app in the kgraph package, but here would be the key steps to get the AUC performance of your model and build a KG.

If you have Docker Compose, you can clone the 'kgraph' repository

```
git clone https://gitlab.com/thomaschln/kgraph
```

Start the docker (it will take some time to build so we can move on and come back to it after the next section)

```
cd kgraph
docker compose up -d shiny
```

#### Computing embeddings on CUIs

These are the previously performed steps to import full text, you don't need to recompute it, just here for reference.

```
  dirpath = system.file('extdata', package = 'psychclustRMED24')
  fpath = file.path(dirpath, 'epmc_1700_suic_db.xml')

  # this shouldn't take more than a few minutes on modern hardware,
  # otherwise download the glove_fit file below
  ftext = nlpembeds:::build_ftext_from_pmcxml(fpath, '<articles>', '</articles>')
  processed_text = nlpembeds:::process_pubmed(ftext)
```

Here, when we call `prune_text`, we use the ontologies mapping file to replace matching strings by their CUIs and other concept identifiers. In the NILE software, only CUIs remain.

```
  # this will take >30min, so rather download this copy from Dropbox
  # pruned_text_cuis = nlpembeds:::prune_text(processed_text, replace_by_cuis = TRUE, stem_derivatives = FALSE)
  system('wget -O pruned_text_cuis.rds "https://www.dropbox.com/scl/fi/ktrkykplm96mq0stbay62/pruned_text_cuis.rds?rlkey=qk32o1tgarjeuih62l4to4v2b&dl=0"')
  pruned_text_cuis = get(load('pruned_text_cuis.rds'))
```

If we have a look at the output, we can see some strings were replaced by CUIs and other concepts

```
pruned_text_cuis[1]
```

Now we refit the Glove embeddings

```
  glove_fit_cuis = nlpembeds:::str_to_vectors(pruned_text_cuis)
  # you can find this file in the Dropbox folder, along with the glove fit on 8,000 publications performed using NILE
```

#### Fitting against known pairs

We use the disease relations from PrimeKG. PhD student Hongyi Yuan contributed the mapping file from MONDO ids (used by PrimeKG) to CUIs. In the 'kgraph' package is provided a csv file of ~100k CUI relations.

```
  cuis_embeds = grep('^UMLS_CUI', rownames(glove_fit_cuis), value = TRUE)
  m_embeds = glove_fit_cuis[cuis_embeds, ]

  cuis_embeds = gsub('^UMLS_CUI:', '', cuis_embeds)
  # only 129 CUIs in our embeddings, but already a good starting point

  dirpath = system.file('data', package = 'kgraph')
  df_pairs = get(load(file.path(dirpath, 'df_cuis_pairs.rds')))
  df_pairs_cols = c('umls_id.x', 'umls_id.y')

  dim(subset(df_pairs, umls_id.x %in% cuis_embeds & umls_id.y %in% cuis_embeds))
  # 30 known pairs between CUIs
```

The `fit_embeds_to_pairs` function fits the embedding matrix to the known pairs. To compute AUC performance, an equal number of random pairs are generated to provide 'null' relationships (true negatives). AUC is computed using the 'pROC' package, and the object is stored, so we can easily plot the AUC curve.

```
  rownames(m_embeds) = cuis_embeds
  fit_kg = kgraph:::fit_embeds_to_pairs(m_embeds, df_pairs[df_pairs_cols])

  pROC::plot.roc(fit_kg$roc, print.auc = TRUE)
```

The null relationships are then used to determine a threshold in the cosine similarities. Usually we will set a threshold at 5 or 10% false positives, corresponding to AUC specificity of 0.95 or 0.9. The `fit_embeds_to_pairs` function will then return the data frame of cosine similarities between concepts above that threshold, in the `df_projs` slot. By default it uses 10% to create this data frame, and also returns the 5% threshold, so that we can easily compare after that how choosing either the 5 or 10% threshold impacts our model.

```
  dim(fit_kg$df_projs)
  # ~1000 predicted relationships at 10% FP

  dim(subset(fit_kg$df_projs, weight > fit_kg$threshold_5fp))
  # ~800 predicted relationships at 5% FP
```

We are now ready to visualize our knowledge graph. We select nodes of interest, and plot all concepts with relationships, weighting edges and nodes sizes by the cosine similarities so that more similar nodes are closer to the selected nodes and bigger. We also provide a `df_dict` which acts as a dictionary containing the labels of CUIs and needs to be a data frame with columns 'id', 'color', 'desc' for labels, and optionally 'group' (see vignette).

```
  dirpath = system.file("data", package = "nlpembeds")
  df_cui_mapping = get(load(file.path(dirpath, "df_cui_mapping.rds")))
  df_cui_mapping = subset(df_cui_mapping, preferred_label & !duplicated(id))
  df_cui_mapping$id = gsub('^UMLS_CUI:', '', df_cui_mapping$id)
  df_cui_mapping$desc = df_cui_mapping$label
  df_cui_mapping$color = 'black'
  df_cui_mapping$group = 'CUI'


  # these are the 4 custom CUIs I created to make sure all suicide-related strings found in DSM are mapped
  selected_nodes = paste0('C999999', 6:9)
  tail(df_cui_mapping[1:2], 4)

  kgraph_obj = kgraph:::get_kgraph(selected_nodes, fit_kg$df_projs, df_dict = df_cui_mapping)
```

The `kgraph_obj` object is a list of edges and nodes data frames we can now feed to the 'sgraph' package. As previously, if you are in your local R console this should open the link in your browser:

```
  igraph = sgraph:::l_graph_to_igraph(kgraph_obj)

  # we set clusters to FALSE because we don't have a 'group' column in `df_dict`
  sgraph:::sgraph_clusters(igraph, node_size = "weight", clusters = FALSE,
    label = 'label', color_map = colors_mapping, layout = igraph::layout_with_kk(igraph))
```

#### Kgraph Shiny app

If you built locally your Docker image, you can navigate to [http://localhost:9083](http://localhost:9083)

Otherwise you can try the live app version, kindly provided to you by the CELEHS laboratory and the Parse Health research organization. It normally takes a few seconds to initialize, the embeddings are already computed but we need to fit them to the pairs. Since it will propably be very slow if >10 people click on it at the same time, consider the following: assume a time-range of 2 min, and click on the link according to your last name's first letter, i.e. A clicks right away, M clicks in 1 min, Z clicks in 2 min.

Ready ? 

Go for it: [https://kgraph.parse-health.org](https://kgraph.parse-health.org)

You can notice you can also choose the similarity considered (cosine similarity, inner product...) and the AUC appears in a dedicated section. One improvement would be to select CUIs based on their labels.

# Conclusions

* Our results point to the fact that sleep is a very important feature, maybe the most important.
* We have shown ways to integrate concepts from various sources, which include "borderline" clinically-relevant concepts as guilt, shame, apprehension.
* We have put in evidence less well-known psychiatric diagnoses, as well as important medical concepts that are not psychiatric diagnoses: anhedonia, dissociation, amusia, anancastic neurosis

# Discussion

* As stated above, we are now using the discovered features as input for other NLP algorithms in our suicide risk prediction project.
* Considering our results and Prof. Terra's suicidal crisis model, we might want to open research in the directions of relapses and intentionality degrees (instead of likelihood).
* We are also investigating the use of embeddings from pre-trained language models as Bert and BGE.

# Feedback, Q&A

Q: Dropbox link only accessible to business accounts
A: Added epmc_1700.xml.gz in Gitlab repository

Q: For researchers analyzing similar addiction related EMR, how could one manage: 1) the fact that clinicians may avoid using explicitly drug names and use slang or allusions instead; 2) the fact that different clinicians will each have their own allusions and style of writing ?
A: Similar problematics in suicide related research, as clinicians may avoid being explicit. Hard question to solve, using embeddings probably the way forward but too early to give further answers. May point to the importance of being able to train our own models on our own data, as pre-trained models on general corpora may be less able to figure out such context-specific allusions.

Q: Resources for beginners ?
A: The text2vec package has 2 vignettes. One focuses on text tokenization and vocabulary building, which can enable to try out the different parameters as minimum term frequency. The other vignette focuses on Glove word embeddings and shows how to use the skipgram window parameter for example, which corresponds to the size of the sliding window used by word2vec.


