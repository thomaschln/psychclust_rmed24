# prepare the package for release
PKGNAME := $(shell sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS := $(shell sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGSRC  := $(shell basename `pwd`)

all: clean devtools_check

doc.pdf:
	R CMD Rd2pdf -o doc.pdf .

build:
	cd ..;\
	R CMD build --no-manual $(PKGSRC)

build-cran:
	cd ..;\
	R CMD build $(PKGSRC)

install: build
	cd ..;\
	R CMD INSTALL $(PKGNAME)_$(PKGVERS).tar.gz

check: build-cran
	cd ..;\
	R CMD check $(PKGNAME)_$(PKGVERS).tar.gz --as-cran

devtools_check:
	R -e "devtools::check()"

vignette:
	cd vignettes;\
	R -e "Sweave('psychclustRMED24.Rnw');tools::texi2pdf('psychclustRMED24.tex')"

clean:
	$(RM) doc.pdf
	cd vignettes;\
	$(RM) *.pdf *.aux *.bbl *.blg *.out *.tex *.log

roxygenise:
	R -e "roxygen2::roxygenise()"

devtools_test:
	R -e "devtools::test()"

download_extdata:
	R -e "psychclustRMED24:::download_extdata()"

db0:
	R -e "psychclustRMED24:::build_db0()"

db1:
	R -e "psychclustRMED24:::build_db1()"

db2:
	R -e "psychclustRMED24:::build_db2()"

db3:
	R -e "psychclustRMED24:::build_db3()"
